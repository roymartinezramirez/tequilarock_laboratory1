package com.cynergy.tequilarock_lab

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcessing.setOnClickListener {
            val age = etAge.text.toString().toIntOrNull()

            if(age != null){
                if (age < 18) tvResult.text = getString(R.string.MINOR)
                else tvResult.text = getString(R.string.OLDER)
            } else {
                Toast.makeText(applicationContext, "Debe ingresar un número", Toast.LENGTH_SHORT).show()
            }
        }
    }
}